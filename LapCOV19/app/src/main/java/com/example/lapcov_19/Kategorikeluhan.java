package com.example.lapcov_19;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Kategorikeluhan extends AppCompatActivity {
Button Back;
Button Back1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kategorikeluhan);
        Back = findViewById(R.id.btnkel1);
        Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent about = new Intent(Kategorikeluhan.this, Detailkeluhan.class);
                startActivity(about);
            }
        });
        Back1 = findViewById(R.id.btnkel2);
        Back1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent about = new Intent(Kategorikeluhan.this, Riwayatkeluhan.class);
                startActivity(about);
            }
        });
    }
}
